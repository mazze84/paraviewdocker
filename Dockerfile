FROM ubuntu:latest 

# Dockerfile author / maintainer 
LABEL maintainer="Matthias Lange <Matthias.lange@utah.edu> "

RUN apt-get update -y && apt-get install -y python  wget iproute2 && mkdir /opt/paraview/ 
WORKDIR /opt/paraview

ARG pvVersion=5.6.0



RUN wget "http://www.paraview.org/paraview-downloads/download.php?submit=Download&version=v5.6&type=binary&os=Linux&downloadFile=ParaView-"${pvVersion}"-osmesa-MPI-Linux-64bit.tar.gz" -O paraview.tar.gz && tar -xf paraview.tar.gz
ENV PATH="/opt/paraview/ParaView-${pvVersion}-osmesa-MPI-Linux-64bit/bin:${PATH}"
Add ./start.sh /opt/paraview/start.sh

ENTRYPOINT ["/opt/paraview/start.sh",""]
