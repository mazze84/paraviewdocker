#!/usr/bin/env bash
set -e

echo "Start Paraview container. The following Ip addresses are assigned to me:"
ip a | grep -oE "\b([0-9]{1,3}\.){3}[0-9]{1,3}\b"
pvserver --multi-clients --mpi

